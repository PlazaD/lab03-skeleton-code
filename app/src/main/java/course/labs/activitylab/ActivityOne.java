package course.labs.activitylab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

  // string for logcat documentation
  private final static String TAG = "Lab-ActivityOne";

  // lifecycle counts
  private int onCreateCount = 0;
  private int onStartCount = 0;
  private TextView onStartTextView;
  private int onResumeCount = 0;
  private TextView onResumeTextView;
  private int onPauseCount = 0;
  private TextView onPauseTextView;
  private int onStopCount = 0;
  private TextView onStopTextView;
  private int onDestroyCount = 0;
  private TextView onDestroyTextView;
  private int onRestartCount = 0;
  private TextView onRestartTextView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_one);

    // Log cat print out
    Log.i(TAG, "onCreate called");
    onCreateCount++;
    ((TextView) findViewById(R.id.create)).setText(getString(R.string.onCreate, onCreateCount));


    onStartTextView   = (TextView) findViewById(R.id.start);
    onStartTextView.setText(getString(R.string.onStart, onStartCount));

    onResumeTextView  = (TextView) findViewById(R.id.resume);
    onResumeTextView.setText(getString(R.string.onResume, onResumeCount));

    onPauseTextView   = (TextView) findViewById(R.id.pause);
    onPauseTextView.setText(getString(R.string.onPause, onPauseCount));

    onStopTextView    = (TextView) findViewById(R.id.stop);
    onStopTextView.setText(getString(R.string.onStop, onStopCount));

    onDestroyTextView = (TextView) findViewById(R.id.destroy);
    onDestroyTextView.setText(getString(R.string.onDestroy, onDestroyCount));

    onRestartTextView = (TextView) findViewById(R.id.restart);
    onRestartTextView.setText(getString(R.string.onRestart, onRestartCount));
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.activity_one, menu);
    return true;
  }

  // lifecycle callback overrides

  @Override
  public void onStart() {
    super.onStart();

    // Log cat print out
    Log.i(TAG, "onStart called");
    onStartCount++;
    onStartTextView.setText(getString(R.string.onStart, onStartCount));
  }

  @Override
  public void onResume() {
    super.onResume();

    // Log cat print out
    Log.i(TAG, "onResume called");
    onResumeCount++;
    onResumeTextView.setText(getString(R.string.onResume, onResumeCount));
  }

  @Override
  public void onPause() {
    super.onPause();

    // Log cat print out
    Log.i(TAG, "onPause called");
    onPauseCount++;
    onPauseTextView.setText(getString(R.string.onPause, onPauseCount));
  }

  @Override
  public void onStop() {
    super.onStop();

    // Log cat print out
    Log.i(TAG, "onStop called");
    onStopCount++;
    onStopTextView.setText(getString(R.string.onStop, onStopCount));
  }

  @Override
  public void onDestroy() {
    super.onDestroy();

    // Log cat print out
    Log.i(TAG, "onDestroy called");
    onDestroyCount++;
    onDestroyTextView.setText(getString(R.string.onDestroy, onDestroyCount));
  }

  @Override
  public void onRestart() {
    super.onRestart();

    // Log cat print out
    Log.i(TAG, "onRestart called");
    onRestartCount++;
    onRestartTextView.setText(getString(R.string.onRestart, onRestartCount));
  }

  @Override
  public void onSaveInstanceState(Bundle savedInstanceState) {
    // TODO: save state information with a collection of key-value pairs & save all  count variables
  }


  public void launchActivityTwo(View view) {
    startActivity(new Intent(this, ActivityTwo.class));
  }


}
